﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace proc.func
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			Add(3, 7);

			var r = Add(9, 15);		//24. Note Add method is static. You can call it by prepending its className: "UnitTest1.Add(9,15)". But "UnitTest1." can be omitted here because we are in that class right now.

			var r1 = new UnitTest1().multi(7, 8);	//56. Note multi is an instance method, so must use an instance to call this method.

		}

		static public int Add(int a, int b) {
			return a + b;
		}

		public int multi(int x, int y) {
			return x * y;
		}
	}
}
