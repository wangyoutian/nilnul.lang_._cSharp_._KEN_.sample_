﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace proc.block
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			///right click to Debug Test
			///go step by stemp
			var a = 2;  //note: the compiler will deduce that "var" is "int" because 0 is int.
			var b = 3;
			
			{
				a = 6;

			}
			Debug.WriteLine($"a:{a},b:{ b}");

		}
		/// see your test-explorer tab. you can open your "test explorer" tab from your visual studio menu "Test"
	}
}
