﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace proc.try0
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			///debug this test (right click to debug ); execute step by step
			try
			{
				int a;
				a = 3;      //now a is what?

				int b = a / 0;	//throw divideByZero exception

				///exception is throw before this. so following statements will not be executed.
				throw new NullReferenceException();

				a = 4;		//this will not executed as exception is thrown before this

			}
			catch (NullReferenceException ex)
			{
				Debug.WriteLine(ex.Message);
			}
			catch (Exception)
			{

				
			}
			finally
			{
				Debug.WriteLine("in finally");
			}
			Debug.WriteLine("after finally");
			/// after this, see your "Test Explorer" tab.

		}
	}
}
